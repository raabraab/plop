<?php 
	/***	
	Copyright (C) <2019>  <Thomas Raab>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
 	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Please send all Inquires to: tom@traab.com

	***/
date_default_timezone_set('America/New_York');
/*** RUN FILE ***/
$cache_override = 0; // minutes - moving to the template engine
$page_template = "default/template1";
$page_title = "Plop Template Engine";

function startSession() {
	$time = 36000;
	$ses = 'PLOP';// this can be renamed or moved off to the configs
	session_set_cookie_params($time);
	//session_name($ses);
	session_start();

	// Reset the expiration time upon page load
	if (isset($_COOKIE[$ses]))
		setcookie($ses, $_COOKIE[$ses], time() + $time, "/");
}
startSession();

$GLOBALS["APP_ROOT"] = $_SERVER["DOCUMENT_ROOT"]."../app";
require_once($GLOBALS["APP_ROOT"]."/classes/template_engine.php");
require_once($GLOBALS["APP_ROOT"]."/classes/file_writer.php");
$plop = new TemplateEngine();
$plop->setTemplate($page_template);

?>