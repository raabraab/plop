<?php 
		/***	
	Copyright (C) <2019>  <Thomas Raab>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
 	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Please send all Inquires to: tom@ploppityplop.com

	***/
	
	// Cache the output to a file
	$cache_file_object = new file_writer($cachefile,"wb",ob_get_contents());
	ob_end_clean(); // never flush to browser	
	// now include the cache file
	include($cachefile);

?>