<?php /*** Template Engine ***/

	/***	
	Copyright (C) <2019>  <Thomas Raab>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
 	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Please send all Inquires to: tom@ploppityplop.com

	***/

class TemplateEngine{

	private $page_style_list = "";
	private $page_metaData = array();
	private $page_scripts = array();
	private $page_content = array();
	private $pageContent = array();
	private $engineOutput = "";
	private $engineComment = "";
	private $template_comments = array();
	private $pageHeaders = array();
	private $app_root = "../";
	private $cache = 0;
	private $template = "";

	public function addMetaData($name,$content){
		global $page_content;
		
		if (strpos($name, 'og:') !== FALSE)
			$value = "<meta property=\"".$name."\" content=\"".$content."\">";
		else	
			$value = "<meta name=\"".$name."\" content=\"".$content."\">";
		
		array_push($page_content, "snip_metaData|".$value);
		return;
	}

	// All modules are run and the results are stored and inserted into the pageContent array
	// column is just a named template location where you can stack modules
	public function module($column,$path){
		ob_start();
		$this->includeFile($path);
		$modContent = ob_get_clean();
		$modArray = array($column,$modContent);
		array_push($this->pageContent,$modArray);
		ob_end_clean();
		return;
	}
	
	// Called from the defined templates
	public function returnModules($column){
		echo"<!-- returnModules $column -->";
		global $pageContent;
		for($i = 0; $i < count($this->pageContent); $i++){
			$contentArray = $this->pageContent[$i];
			echo"<!-- $contentArray[0] -->";
			if($contentArray[0] == $column){
				print $contentArray[1];	
			}
		}
	}
	
	public function setTemplate($path){
		$this->template = $path;
	}
	
	public function getTemplate() {
		return $this->template;
	}
	
	public function setComment($txt){
		array_push($this->template_comments, $txt);
	}
	
	private function returnComments(){
		for($i = 0; $i < count($this->template_comments);$i++){
			print("<!--".$this->template_comments[$i]."-->");
		}
	}

	// Deprecated module function///////////////////////////////
	/*
	function addModule0($column, $path){
		global $page_content;
		array_push($page_content, $column."|".$path);
		return;
	}

	function returnModules0($column){
		
		global $page_content;
		for($i = 0; $i < count($page_content); $i++){
			$contentArray = explode("|",$page_content[$i]);
			if($contentArray[0] == $column){
				//echo $contentArray[1];
				includeFile($contentArray[1]);		
			}
		}
		return;	
	}
	*/
	///////////////////////////////////////////////////////////

	private function includeFile($filename){
		// One pass processing of the model and view files
		// the resulting text is added to the pageContent array
		$strCheck = "/";
		$intpos = strrpos($filename,$strCheck);
		$strpos = (string)$intpos;
		//echo "the last position of ".$strCheck." is ".$strpos."<br>";	
		$specificFile = substr($filename , $intpos+1);
		//echo "the specific file is ".$specificFile."<br>";
		$filepath = $GLOBALS["APP_ROOT"]."".$filename."/".$specificFile."_model.php";
		if (file_exists($filepath)) {
			include($filepath);
		} else {
	   		echo "The file $filepath does not exist";
		}
		$filepath = $GLOBALS["APP_ROOT"]."".$filename."/".$specificFile."_view.html";
		if (file_exists($filepath)) {
			include($filepath);
		} else {
	   		echo "The file $filepath does not exist";
		}	
	}

	
	public function addStyle($nstyle){
		global $page_style_list;
		$page_style_list .= "\t\t\t".$nstyle."\n";
	}

	function returnStyleList() {
		global $page_style_list;
		if($page_style_list != ""){
			print("<style type=\"text/css\">"."\n");
			print($page_style_list);
			print("\t\t"."</style>"."\n");
		}
	}
	
	function addSnippet($column, $snip){
		global $page_content;
		array_push($page_content, "snip_".$column."|".$snip);
		return;	
	}

	public function returnSnippet($column){
		global $page_content;
		for($i = 0; $i < count($page_content); $i++){
			$contentArray = explode("|",$page_content[$i]);
			if($contentArray[0] == "snip_".$column){
				//echo $contentArray[1];
				print($contentArray[1]."\n\t\t");		
			}
		}
		print("\n");
		return;	
	}

	private function clearCacheKey($key){
		$filename = $GLOBALS["APP_ROOT"]."/includes/cache/files/".$key;
		if(file_exists($filename))
			unlink($filename);
	}
	
	public function setCache($cacheTime){
		$this->cache = $cacheTime;
	}

	public function render(){
		global $engineOutput;
		$repChars = array("/",".","?","&","=");
		$cacheKey = str_replace($repChars, "_", $_SERVER["REQUEST_URI"]);
		$cacheKey = str_replace("__flushCache_true","",$cacheKey);
		$engineComment = "<!-- cacheKey: ".$cacheKey." "."generated: ".date('m/d/y H:i')." -->";
		if(isset($_GET["_flushCache"])){
			$cacheBust = $_GET["_flushCache"];
			if($cacheBust == "true"){
				$engineOutput = "<div>CACHE BUSTING</div>";
				$this->clearCacheKey($cacheKey);	
			}
		}
		if($this->cache == 0){
			$engineOutput = "<div style=\"position:absolute; color:#c0d4ec;\">NOCACHE</div>";
		}else{
			$engineOutput = "<div style=\"position:absolute; color:#c3d7ef;\">".$this->cache."</div>";
			$cache_override = $this->cache;
		}
		if($this->cache != 0)
			include($GLOBALS["APP_ROOT"]."/includes/cache/start.php");
		include($GLOBALS["APP_ROOT"]."/templates/".$this->template.".html");
		if($this->cache != 0)
			include($GLOBALS["APP_ROOT"]."/includes/cache/end.php");
	}
}

?>