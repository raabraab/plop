<?php 

	/***	
	Copyright (C) <2019>  <Thomas Raab>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
 	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Please send all Inquires to: tom@ploppityplop.com

	***/

	class file_writer {		
		//var $filename;// = "logs/testlog.txt";
		//var $mode; // = "ab";
		//var $content; 
		function file_writer($filename,$mode,$content){			// In our example we're opening $filename in append mode.		   // The file pointer is at the bottom of the file hence			// that's where $somecontent will go when we fwrite() it.			if (!$handle = fopen($filename, $mode)) {				echo "Cannot open file ($filename)";			   exit;			}			
			if(flock($handle, LOCK_EX)){			   	// Write $somecontent to our opened file.			   if (fwrite($handle, $content) === FALSE) {			       echo "Cannot write to file ($filename)";			       exit;			   }
			   flock($handle, LOCK_UN);			}			 //echo "Success, wrote ($somecontent) to file ($filename)";			fclose($handle);		} 
	}
	
?>