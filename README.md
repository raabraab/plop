# plop

---

The father and daughter development team of Tom and Sarah Raab presents plop, a lightweight OpenSource TMVC framework for PHP

---
### Installation:

##### GIT Repository 

Plop will be distributed through BitBucket. You will be able to clone the project to your local web server directory with the following command:

~~~
git clone https://bitbucket.org/raabraab/plop.git
~~~

##### Update Hosts file

Add a line to your hosts file for your local development domain. 

~~~
127.0.0.1    dev.plop.com
~~~

##### Update Apache Config

Add a Virtual Host configuration to your apache server. Make sure that DocumentRoot and Directory path is set to the webclient directory within the plop project. Also, that the ServerName matches the server added to the hosts file.

~~~
<VirtualHost *:80>
        DocumentRoot "/PathtoYour/WebServer/plop/webclient/
        ServerName dev.plop.com
        <Directory "/PathtoYour/WebServer/plop/webclient/">
                Options FollowSymLinks Multiviews
                MultiviewsMatch Any
                AllowOverride All
                Require all granted
                Order allow,deny
                Allow from all
        </Directory>
</VirtualHost>
~~~

---

### Getting Started

##### Templates
These files are the heart of plop. It's where developers define the pagecode remnants that, knitted together, will be the output of your site. Templates are part of the plop structure and are located in the /app/templates... directory. Any suborganization under templates is up to the site architects' discretion. Templates create the page wrappers as well as define module positions.

~~~
<html> 
    <head>
        <title><? print $plop->getPageTitle(); ?></title>
        <? $plop->returnSnippets("metaData"); ?>    
    </head>
    <body>
        <div id="top">
            <? $plop->returnModules("top"); ?>
        </div>    
    </body>
</html>
~~~

#### Modules (Model-View Files)
Modules are represented by a directory name. This directory is expected to contain two files that represent the model and the view for this content block.
##### Models
The model files are the means to access your data and create the modules data object. We don't presume to know how you access your data, but the simplest method utilizing MySQL in PHP 5.3 would look like the following. The module object is available to this modules view, and to the template. The model file is located in the module directory /app/modules/{optional sub directories}/[moduleName]. The file is then named [moduleName]_model.html

~~~
<?php
    $moduleNameArray;    
    $sql = "SELECT * FROM `content`";
    $result = runQuery($sql);
    while($row = mysql_fetch_array($result,MYSQL_ASSOC))
        array_push($moduleNameArray, $row);
?>
~~~

##### Views
The view files convert the module data object from the above model into html output. This will create a well formed html fragment which is then inserted into the template. The view file is located in the module directory /app/modules/{optional sub directories}/[moduleName]. The file is then named [moduleName]_view.html

~~~
<div id="moduleName" class="content-list-module">
    <?php for($i = 0; $i < count($moduleNameArray); $i++) ?>
        <div class="content-item" >
            <div class="content-image">
                <a href="<? print($moduleNameArray[$i]["link"];?>">
                    <? print($moduleNameArray[$i]['image']);?>
                </a>				
            </div>
            <div class="content-content">
                <div class="date"><? print $moduleNameArray[$i]["date"] ; ?></div>		
                <div class="title"><? print $moduleNameArray[$i]["title"] ; ?></div>
                <div class="copy"><? print $moduleNameArray[$i]["copy"] ; ?></div>	
            </div>
        </div>
    <?  ?>
    </div>
</div>
~~~

##### Controllers
The webclient folder is your web site's document root. Every file in this area is available to the public, so be careful of the sorts of code you put here. The URL endpoints have files that can be considered a controller. This file will specify a template, and then position the modules indicated. They can overwrite a few default settings and set cache values.

~~~
<?php 
    require_once(_SERVER["DOCUMENT_ROOT"]."../app/includes/_run.php");
    $plop->cache(20); 
    $plop->template("/templates/default/template1");
    $plop->module("top", "/modules/plop/logo");
    $plop->module("top", "/modules/plop/about");
    $plop->render();
?>
~~~
The above controller is an example of an index file that controls a page. It does four things: includes the plop _run file; adds a cache time override; adds two Modules, "logo" and "about" to the template location "Top", specifying the module paths as "/modules/plop/logo" and "/modules/plop/about" respectively; and finally, a render call is made to the $plop object where the plop template_engine class puts it all together. The paths specified in these method calls are relative to the sites /app directory which itself resides outside of the web root.

In addition to the site building files, there is the _run.php file that gets required_once on every page. This file is located at /app/include/_run.php. This file adds any site-specific includes, as well as sets up a few global params and instantiates the $plop object. There is also a session code contained here if needed.

##### Flat File Cache


Flat file caching is built in. Before enabling cache (or after) you might need to open directory permissions temporarily, so that plop can create the folder where it will write files. On the command line, navigate to the .../app/includes directory:

~~~
$ cd /Library/Webserver/plop/app/includes/
~~~
Then open permissions for the cache/ directory with the following command:
~~~
$ chmod 777 cache/
~~~
Load a sitepage that has caching enabled. After your sitepage is fully loaded, check inside of the cache directory to see if a "../app/includes/cache/files" directory has been created. If the files directory is in place you will need to reset the permissions on the cache directory with the following command...
~~~
$ chmod 755 cache/
~~~

Flat file caching is now enabled. The application will place flattened versions of each url that has cache values above zero into this folder. Deleting any file here will cue the application to rebuild cache for that url.

---
Plop is based on convention over configuration, but we don't want to tell you what can and can't do. We know from experience that if it can be imagined, it can be built. Plop gets you started, then it stays out of your way, letting you do whatever you see fit.

---

Please send all inquireries to:
~~~
plop@traab.com
~~~